hide everything
show cartoon
set_color colordefault, [0.75,0.75,0.58]
color colordefault, all
set_color blue0, [0.63,0.63,0.63]
set_color blue50, [0.5,0.58,0.68]
set_color blue60, [0.42,0.55,0.71]
set_color blue70, [0.35,0.52,0.73]
set_color blue80, [0.28,0.49,0.76]
set_color blue90, [0.2,0.46,0.8]
set_color blue95, [0.12,0.43,0.83]
set_color blue98, [0.0,0.38,0.87]
set_color blue100, [0.6,0,1]
set_color green0, [0.63,0.63,0.63]
set_color green50, [0.5,0.68,0.56]
set_color green60, [0.42,0.71,0.53]
set_color green70, [0.35,0.74,0.49]
set_color green80, [0.26,0.77,0.44]
set_color green90, [0.19,0.8,0.41]
set_color green95, [0.12,0.83,0.37]
set_color green98, [0.01,0.87,0.31]
set_color green100, [0,1,0.83]
set_color yellow0, [0.63,0.63,0.63]
set_color yellow50, [0.66,0.67,0.51]
set_color yellow60, [0.68,0.71,0.43]
set_color yellow70, [0.7,0.73,0.36]
set_color yellow80, [0.72,0.76,0.28]
set_color yellow90, [0.74,0.8,0.2]
set_color yellow95, [0.76,0.83,0.12]
set_color yellow98, [0.79,0.87,0.0]
set_color yellow100, [1,0.8,0.16]
set_color red0, [0.63,0.63,0.63]
set_color red50, [0.73,0.55,0.55]
set_color red60, [0.75,0.47,0.47]
set_color red70, [0.77,0.38,0.38]
set_color red80, [0.79,0.29,0.29]
set_color red90, [0.82,0.21,0.21]
set_color red95, [0.84,0.13,0.13]
set_color red98, [0.88,0,0]
set_color red100, [1,0,0.55]
color blue0, chain A
select 50pct_grp_2_A, (chain A & resi 27,32,58,79,97,101,117,127,154,155,170,171,192,211,237,259,277,281,286,288,290,292,293,297,315,330,332,352,372)
color blue50, 50pct_grp_2_A
select 60pct_grp_3_A, (chain A & resi 21,23,59,140,144,160,165,186,195,206,222,232,248,255,261,269,301,309,310,331,336,369,375)
color blue60, 60pct_grp_3_A
select 70pct_grp_4_A, (chain A & resi 30,51,80,94,96,123,134,203,215,229,235,249,257,263,265,289,296,303,317,334,341,346,361,362,377)
color blue70, 70pct_grp_4_A
select 80pct_grp_5_A, (chain A & resi 37,84,141,168,183,191,194,212,217,234,241,278,284,307,313,340,348,364,368,374,379)
color blue80, 80pct_grp_5_A
select 90pct_grp_6_A, (chain A & resi 36,90,116,175,177,213,219,246,370)
color blue90, 90pct_grp_6_A
select 95pct_grp_7_A, (chain A & resi 33,35,40,82,178,193,199,218,311,366)
color blue95, 95pct_grp_7_A
select 98pct_grp_8_A, (chain A & resi 34,38,43,78,335,344)
color blue98, 98pct_grp_8_A
select 100pct_grp_9_A, (chain A & resi 91,148,202,204,226,227,233,252,342,347,349,360)
color blue100, 100pct_grp_9_A
