hide everything
show cartoon
set_color colordefault, [0.75,0.75,0.58]
color colordefault, all
set_color blue0, [0.63,0.63,0.63]
set_color blue50, [0.5,0.58,0.68]
set_color blue60, [0.42,0.55,0.71]
set_color blue70, [0.35,0.52,0.73]
set_color blue80, [0.28,0.49,0.76]
set_color blue90, [0.2,0.46,0.8]
set_color blue95, [0.12,0.43,0.83]
set_color blue98, [0.0,0.38,0.87]
set_color blue100, [0.6,0,1]
set_color green0, [0.63,0.63,0.63]
set_color green50, [0.5,0.68,0.56]
set_color green60, [0.42,0.71,0.53]
set_color green70, [0.35,0.74,0.49]
set_color green80, [0.26,0.77,0.44]
set_color green90, [0.19,0.8,0.41]
set_color green95, [0.12,0.83,0.37]
set_color green98, [0.01,0.87,0.31]
set_color green100, [0,1,0.83]
set_color yellow0, [0.63,0.63,0.63]
set_color yellow50, [0.66,0.67,0.51]
set_color yellow60, [0.68,0.71,0.43]
set_color yellow70, [0.7,0.73,0.36]
set_color yellow80, [0.72,0.76,0.28]
set_color yellow90, [0.74,0.8,0.2]
set_color yellow95, [0.76,0.83,0.12]
set_color yellow98, [0.79,0.87,0.0]
set_color yellow100, [1,0.8,0.16]
set_color red0, [0.63,0.63,0.63]
set_color red50, [0.73,0.55,0.55]
set_color red60, [0.75,0.47,0.47]
set_color red70, [0.77,0.38,0.38]
set_color red80, [0.79,0.29,0.29]
set_color red90, [0.82,0.21,0.21]
set_color red95, [0.84,0.13,0.13]
set_color red98, [0.88,0,0]
set_color red100, [1,0,0.55]
color green0, chain A
select 50pct_grp_2_A, (chain A & resi 22,26,51,98,156,175,181,193,231,233,238,259,263,298,334,361,363)
color green50, 50pct_grp_2_A
select 60pct_grp_3_A, (chain A & resi 9,17,33,35,45,62,83,85,111,165,177,195,219,224,239,243,276,318,323,326)
color green60, 60pct_grp_3_A
select 70pct_grp_4_A, (chain A & resi 28,44,47,79,129,143,178,202,228,232,234,274,304,309,313,321)
color green70, 70pct_grp_4_A
select 80pct_grp_5_A, (chain A & resi 34,37,46,57,61,94,96,99,108,131,137,139,157,179,180,182,184,185,190,209,212,215,218,225,254,273,277,281,325,327,352)
color green80, 80pct_grp_5_A
select 90pct_grp_6_A, (chain A & resi 20,67,89,97,138,149,160,172,211,250,261,285,316,351,358)
color green90, 90pct_grp_6_A
select 95pct_grp_7_A, (chain A & resi 32,58,90,130,146,148,150,153,161,183,186,198,199,247,258,266,270,296,312,317,347)
color green95, 95pct_grp_7_A
select 98pct_grp_8_A, (chain A & resi 30,41,63,65,66,69,72,75,81,87,155,158,159,162,163,187,196,197,201,203,207,208,240,242,245,248,251,284,301,305,306,315,322,336,338,341,345,348)
color green98, 98pct_grp_8_A
select 100pct_grp_9_A, (chain A & resi 43,60,64,88,112,114,132,134,135,136,166,173,192,194,205,216,222,227,230,241,249,253,255,257,262,288,293,299,300,302,319,324,335,339,340,342)
color green100, 100pct_grp_9_A
color green0, chain C
select 50pct_grp_2_C, (chain C & resi 22,26,51,98,156,175,181,193,231,233,238,259,263,298,334,361,363)
color green50, 50pct_grp_2_C
select 60pct_grp_3_C, (chain C & resi 9,17,33,35,45,62,83,85,111,165,177,195,219,224,239,243,276,318,323,326)
color green60, 60pct_grp_3_C
select 70pct_grp_4_C, (chain C & resi 28,44,47,79,129,143,178,202,228,232,234,274,304,309,313,321)
color green70, 70pct_grp_4_C
select 80pct_grp_5_C, (chain C & resi 34,37,46,57,61,94,96,99,108,131,137,139,157,179,180,182,184,185,190,209,212,215,218,225,254,273,277,281,325,327,352)
color green80, 80pct_grp_5_C
select 90pct_grp_6_C, (chain C & resi 20,67,89,97,138,149,160,172,211,250,261,285,316,351,358)
color green90, 90pct_grp_6_C
select 95pct_grp_7_C, (chain C & resi 32,58,90,130,146,148,150,153,161,183,186,198,199,247,258,266,270,296,312,317,347)
color green95, 95pct_grp_7_C
select 98pct_grp_8_C, (chain C & resi 30,41,63,65,66,69,72,75,81,87,155,158,159,162,163,187,196,197,201,203,207,208,240,242,245,248,251,284,301,305,306,315,322,336,338,341,345,348)
color green98, 98pct_grp_8_C
select 100pct_grp_9_C, (chain C & resi 43,60,64,88,112,114,132,134,135,136,166,173,192,194,205,216,222,227,230,241,249,253,255,257,262,288,293,299,300,302,319,324,335,339,340,342)
color green100, 100pct_grp_9_C
color green0, chain B
select 50pct_grp_2_B, (chain B & resi 22,26,51,98,156,175,181,193,231,233,238,259,263,298,334,361,363)
color green50, 50pct_grp_2_B
select 60pct_grp_3_B, (chain B & resi 9,17,33,35,45,62,83,85,111,165,177,195,219,224,239,243,276,318,323,326)
color green60, 60pct_grp_3_B
select 70pct_grp_4_B, (chain B & resi 28,44,47,79,129,143,178,202,228,232,234,274,304,309,313,321)
color green70, 70pct_grp_4_B
select 80pct_grp_5_B, (chain B & resi 34,37,46,57,61,94,96,99,108,131,137,139,157,179,180,182,184,185,190,209,212,215,218,225,254,273,277,281,325,327,352)
color green80, 80pct_grp_5_B
select 90pct_grp_6_B, (chain B & resi 20,67,89,97,138,149,160,172,211,250,261,285,316,351,358)
color green90, 90pct_grp_6_B
select 95pct_grp_7_B, (chain B & resi 32,58,90,130,146,148,150,153,161,183,186,198,199,247,258,266,270,296,312,317,347)
color green95, 95pct_grp_7_B
select 98pct_grp_8_B, (chain B & resi 30,41,63,65,66,69,72,75,81,87,155,158,159,162,163,187,196,197,201,203,207,208,240,242,245,248,251,284,301,305,306,315,322,336,338,341,345,348)
color green98, 98pct_grp_8_B
select 100pct_grp_9_B, (chain B & resi 43,60,64,88,112,114,132,134,135,136,166,173,192,194,205,216,222,227,230,241,249,253,255,257,262,288,293,299,300,302,319,324,335,339,340,342)
color green100, 100pct_grp_9_B
color green0, chain E
select 50pct_grp_2_E, (chain E & resi 22,26,51,98,156,175,181,193,231,233,238,259,263,298,334,361,363)
color green50, 50pct_grp_2_E
select 60pct_grp_3_E, (chain E & resi 9,17,33,35,45,62,83,85,111,165,177,195,219,224,239,243,276,318,323,326)
color green60, 60pct_grp_3_E
select 70pct_grp_4_E, (chain E & resi 28,44,47,79,129,143,178,202,228,232,234,274,304,309,313,321)
color green70, 70pct_grp_4_E
select 80pct_grp_5_E, (chain E & resi 34,37,46,57,61,94,96,99,108,131,137,139,157,179,180,182,184,185,190,209,212,215,218,225,254,273,277,281,325,327,352)
color green80, 80pct_grp_5_E
select 90pct_grp_6_E, (chain E & resi 20,67,89,97,138,149,160,172,211,250,261,285,316,351,358)
color green90, 90pct_grp_6_E
select 95pct_grp_7_E, (chain E & resi 32,58,90,130,146,148,150,153,161,183,186,198,199,247,258,266,270,296,312,317,347)
color green95, 95pct_grp_7_E
select 98pct_grp_8_E, (chain E & resi 30,41,63,65,66,69,72,75,81,87,155,158,159,162,163,187,196,197,201,203,207,208,240,242,245,248,251,284,301,305,306,315,322,336,338,341,345,348)
color green98, 98pct_grp_8_E
select 100pct_grp_9_E, (chain E & resi 43,60,64,88,112,114,132,134,135,136,166,173,192,194,205,216,222,227,230,241,249,253,255,257,262,288,293,299,300,302,319,324,335,339,340,342)
color green100, 100pct_grp_9_E
color green0, chain D
select 50pct_grp_2_D, (chain D & resi 22,26,51,98,156,175,181,193,231,233,238,259,263,298,334,361,363)
color green50, 50pct_grp_2_D
select 60pct_grp_3_D, (chain D & resi 9,17,33,35,45,62,83,85,111,165,177,195,219,224,239,243,276,318,323,326)
color green60, 60pct_grp_3_D
select 70pct_grp_4_D, (chain D & resi 28,44,47,79,129,143,178,202,228,232,234,274,304,309,313,321)
color green70, 70pct_grp_4_D
select 80pct_grp_5_D, (chain D & resi 34,37,46,57,61,94,96,99,108,131,137,139,157,179,180,182,184,185,190,209,212,215,218,225,254,273,277,281,325,327,352)
color green80, 80pct_grp_5_D
select 90pct_grp_6_D, (chain D & resi 20,67,89,97,138,149,160,172,211,250,261,285,316,351,358)
color green90, 90pct_grp_6_D
select 95pct_grp_7_D, (chain D & resi 32,58,90,130,146,148,150,153,161,183,186,198,199,247,258,266,270,296,312,317,347)
color green95, 95pct_grp_7_D
select 98pct_grp_8_D, (chain D & resi 30,41,63,65,66,69,72,75,81,87,155,158,159,162,163,187,196,197,201,203,207,208,240,242,245,248,251,284,301,305,306,315,322,336,338,341,345,348)
color green98, 98pct_grp_8_D
select 100pct_grp_9_D, (chain D & resi 43,60,64,88,112,114,132,134,135,136,166,173,192,194,205,216,222,227,230,241,249,253,255,257,262,288,293,299,300,302,319,324,335,339,340,342)
color green100, 100pct_grp_9_D
